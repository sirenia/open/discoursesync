# Discourse Sync

Uploads documentaion to Discourse

## Usage

call the function with the path to the config file.

## example

`npx discoursesync --config config.json --username username --key key --categoryName categoryName --categoryId categoryId`

These args can also be placed in the config file.

```JSON
{
  "uploads": [
    { "path": "", "name": "" }
  ],
  "baseURL": "",
  "tagDepricated": false,
  "specialTitleRules": {
    "name": "new_name"
  },
  "http_interval": 1200,
  "http_timeout_interval": 20000,
  "ignore": []
}
```

Here

- `uploads` is an array of the dirs we upload and the name of the subcategory it
  should be placed in eg. `{ "path": "./docs/modules", "name": "Modules" }`.
- `categoryId` is used to ensure that we target the correct category.
- `baseURL` is the URL ending in an `/` eg. `https:...io/`.
- `tagDepricated` tags existing topics in subcategory if they weren't uploaded
  now.
- `specialTitleRules` allows to overwrite documentaion names, useful for names
  that is renamed due to Discourse rules.
- `http_interval` interval of http requests. Notice that the api only accepts 50
  requests pr min, therefore an interval of min 1200 is recommended.
- `http_timeout_interval` interval when facing a timeout.
- `ignore` files it ignores when uploading and tagging.
