#!/usr/bin/env node
"use strict";
import { readdir, readFile } from "node:fs/promises";
import Axios from "axios";
import FormData from "form-data";
import { marked } from "marked";

// Config of the category, id and slug is used to doublecheck that the correct category was found
const argsv = process.argv.slice(2);

const configIndex = argsv.indexOf("--config");
if (configIndex == -1) throw "No config arg found";
const configPath = argsv[configIndex + 1];
if (!configPath) throw "no path argument";
const configJSON = await readFile(configPath);
if (!configJSON) throw "didn't find file";
const config = JSON.parse(configJSON);

const getOrThrow = (name, envName) => {
  const confArg = argsv.indexOf(`--${name}`);
  if (confArg != -1) return argsv[confArg + 1];
  const confFile = config[name];
  if (confFile) return confFile;
  const confEnv = process.env[envName];
  if (!confEnv) throw `config file didn't contain ${name}`;
  return confEnv;
};

const username = getOrThrow("username");
const key = getOrThrow("key", "ASK_KEY");
const categoryName = getOrThrow("categoryName", "ASK_CATEGORY_NAME");
const categoryId = Number(getOrThrow("categoryId", "ASK_CATEGORY_ID"));

// { path: path to a dir, name: Name of the sub category on ask }
const uploads = getOrThrow("uploads");

const baseURL = getOrThrow("baseURL");

const ignore = config.ignore || [];
const tagDeprecatedTopics = config.tagDepricated || false;
const specialTitleRules = config.specialTitleRules || {};
const DEFAULT_INTERVAL = config.http_interval || 2000;
const ERROR_INTERVAL = config.http_timeout_interval || 20000;

/**
 * Variables
 */

let subCategories = {}; // initiated in setup

/**
 * Http setup
 */

const MAX_REQUESTS_COUNT = 1;
let INTERVAL_MS = DEFAULT_INTERVAL;
let PENDING_REQUESTS = 0;

const http = Axios.create({
  baseURL: baseURL,
  headers: {
    "Api-Username": username,
    "Api-Key": key,
    "Content-Type": "application/json",
    Accept: "application/json",
  },
});

http.interceptors.request.use((config) => {
  if (config.data instanceof FormData) {
    Object.assign(config.headers, config.data.getHeaders());
  }
  return new Promise((resolve, _) => {
    let interval = setInterval(() => {
      if (PENDING_REQUESTS < MAX_REQUESTS_COUNT) {
        PENDING_REQUESTS++;
        clearInterval(interval);
        resolve(config);
      }
    }, INTERVAL_MS);
  });
});

http.interceptors.response.use(function (response) {
  setTimeout(() => {
    PENDING_REQUESTS = Math.max(0, PENDING_REQUESTS - 1);
    INTERVAL_MS = DEFAULT_INTERVAL;
  }, 50);
  return Promise.resolve(response);
}, function (error) {
  PENDING_REQUESTS = Math.max(0, PENDING_REQUESTS - 1);
  if (error?.response?.status === 429) {
    // a bit crazy solution, if we send too many requests, just try again (server has a 20 sec cooldown)
    console.log("request timed out, trying again");
    INTERVAL_MS = ERROR_INTERVAL;
    const newReq = http.request(error.config);
    return Promise.resolve(newReq);
  } else {
    console.log(
      "API call failed with code:",
      error.code,
      error.response?.status,
      error.response?.statusText,
      error.response?.data?.errors,
    );
    return Promise.resolve(undefined);
  }
});

const setup = async () => {
  const categories = await http.get(`categories.json`);
  if (!categories) throw "First API call failed";
  const docCategory =
    categories.data.category_list.categories.filter((e) =>
      e.name === categoryName
    )[0];
  if (!docCategory) {
    throw `Didn't find category of the name, expected ${categoryName} in list ${
      categories.data.category_list.categories.map((e) => e.name)
    }`;
  }
  if (docCategory.id !== categoryId) {
    throw `ID's doesn't match`;
  }
  const temp = await Promise.all(
    docCategory.subcategory_ids.map((id) => http.get(`c/${id}/show.json`)),
  );
  subCategories = keyBy(temp.map((e) => e.data.category), "name");
};

marked.use({
  extensions: [{
    name: "box",
    level: "block",
    start(src) {
      return src.match(/^:::([\s\S]*?):::/)?.index;
    },
    tokenizer(src, _tokens) {
      const match = src.match(/^:::([\s\S]*?):::/);
      if (match) {
        const token = {
          type: "box",
          raw: match[0],
          text: match[1].trim(),
          tokens: [],
        };
        this.lexer.inline(token.text, token.tokens); // Queue this data to be processed for inline tokens
        return token;
      }
    },
    renderer(token) {
      let e = token.text;
      const typ = e.split(/[\n\s]/g).filter((e) => e)[0];
      const color = getColor(typ);
      if (e.split("\n")[0].trim() != typ) e = e.replace(typ, "");
      const title = e.split("\n")[0];
      e = e.replace(title, "<h4>" + title + "</h4>");
      return `<div data-box="${color}" > ${marked.parse(e)} </div>`;
    },
  }, {
    name: "youtubeLink",
    level: "block",
    start(src) {
      return src.match(/(^\@\[youtube\]\()(.*)(\))/)?.index;
    },
    tokenizer(src, _tokens) {
      const match = src.match(/(^\@\[youtube\]\()(.*)(\))/);
      if (match) {
        return {
          type: "youtubeLink",
          raw: match[0],
          link: match[2],
        };
      }
    },
    renderer(token) {
      return `\n\nwww.youtube.com/watch/?v=${token.link}\n\n`;
    },
  }],
});

const renderer = {
  code(code, info) {
    return `\n\n\`\`\`${info}\n${code}\n\`\`\`\n`;
  },
  codespan(code) {
    return `\`${code}\``;
  },
  paragraph(text) {
    return text.split("\n\n").map((e) =>
      e.replaceAll("\n", " ").replaceAll("<lbr>", "\n")
    ).join("\n\n"); // prevent discourse from seeing this as two lines
  },
  image(href, title, text) {
    return `![${text}](${href}${title ? " " + title : ""})`;
  },
  heading(text, level, raw) {
    return "#".repeat(level) + " " + text + "\n\n";
  },
};

const walkTokens = (token) => {
  if (token.type == "list") {
    token.type = "text";
    token.text = "\n\n" + token.raw.replaceAll("\n", "<lbr>") + "\n\n";
  }
  if (token.type == "space") {
    token.type = "text";
    token.text = token.raw;
  }
};

marked.use({ renderer, walkTokens });

const getColor = (typ) => {
  switch (typ && typ.trim()) {
    case "warning":
      return "orange";
    case "tip":
      return "purple";
    default:
      return "grey";
  }
};

/**
 * Methods
 */

const keyBy = (arr, by) => {
  return arr.reduce((acc, e) => {
    acc[e[by]] = e;
    return acc;
  }, {});
};

const addSlugAndIdToDir = (dirs) => {
  return dirs.map((e) => {
    const { slug, id } = subCategories[e.name];
    return { ...e, slug, id };
  });
};

const replaceImages = async (md, path) => {
  const regex = /(!\[.*]\()(.*)(\))/g;

  const imgs = [...md.matchAll(regex)].map((e) => e[2]);
  const newImgs = await Promise.all(imgs.map((e) => uploadImg(path + e)));
  const temp = imgs.map((_, i) => {
    return { old: imgs[i], newSrc: newImgs[i] };
  });
  const map = keyBy(temp, "old");
  const res = md.replaceAll(
    regex,
    (_, a, path, c) => `${a}${map[path].newSrc}${c}`,
  );

  await res;
  return res;
};

const mdToPost = async (md, path) => {
  let config = md.match(/---[\S\s]*?---/g);
  let title = "title_not_found";
  if (config) {
    config = config[0];
    const temp_title = config.match(/title: (.*)/);
    const version_temp = config.match(/version: "*([^"\n]*)"*/);
    const version = version_temp ? version_temp[1]?.trim() : undefined;
    md = md.replace(config, "").trim();
    if (temp_title[1]) {
      title = `${temp_title[1]} ${version ? version : ""}`.trim();
    }
  } else {
    const title_temp = md.match(/# (.*)/);
    if (title_temp[1]) title = title_temp[1];
  }
  title = specialTitleRules[title] || title;
  if (ignore.includes(title)) return undefined;
  let text = await replaceImages(md, path);
  const html = marked.parse(text);
  return { title, text: html };
};

const recReader = async (fromDir) => {
  if (fromDir.path.endsWith(".md")) {
    const path = fromDir.path;
    const name = path.slice(Math.max(0, path.lastIndexOf("/") + 1));
    const file = await readFile(path, { encoding: "utf8" });
    return [await mdToPost(file, path.replace(name, ""))];
  } // create a topic for each dir
  const res = [];
  const dirReader = async (path) => {
    let files = await readdir(path);
    files = files.map((e) => e.toString());
    await Promise.all(files.map(async (e) => {
      if (e.endsWith(".md")) {
        const file = await readFile(path + "/" + e, { encoding: "utf8" });
        res.push(await mdToPost(file, path + "/"));
      } else if (!e.includes(".")) {
        await dirReader(path + "/" + e);
      }
    }));
  };

  await dirReader(fromDir.path);
  return res.filter((e) => e); // remove empty if ignored
};

/**
 * Http methods
 */

const getTopics = async (c) => {
  // api atmost returns 30 topics, so we just request until we receive less than 30
  let list = [];
  let page = 0;
  while (true) {
    const url = `c/${c.slug}/${c.id}.json?ascending=false&page=${page}`;
    let res = await http.get(url);
    if (!res) return undefined;
    page++;
    const newTopics = res.data.topic_list.topics;
    list = list.concat(newTopics);
    if (newTopics.length < 30) return list;
  }
};

const createTopic = async (categoryId, title, content) => {
  const form = { "title": title, "raw": content, "category": categoryId };
  const res = await http.post("posts.json", JSON.stringify(form));
  if (res) console.log("created topic: ", title);
  else console.log("failed to create topic: ", title);
  return res;
};

const getTopic = async (id) => {
  const res = await http.get(`t/${id}.json`);
  if (!res) console.log("didn't get posts from topic ", id);
  return { posts: res?.data.post_stream.posts, topic: res.data };
};

const editFirstPostOnTopic = async (topicId, newContent) => {
  const temp = await getTopic(topicId);
  const posts = temp.posts;
  const post = posts ? posts[0] : undefined;
  const content = { post: { raw: newContent } };
  if (!post) {
    console.log("didn't edit ", topicId);
    return undefined;
  }
  const res = await http.put(`posts/${post.id}.json`, JSON.stringify(content));
  if (res) console.log("edited topic: ", topicId);
  else console.log("failed edit for topic", topicId);
  if (temp.topic?.tags?.includes("deprecated")) {
    let topic = temp.topic;
    topic.tags = topic.tags.filter((e) => e != "deprecated");
    const resRemoveDeprecated = await http.put(
      `t/-/${topic.id}.json`,
      JSON.stringify(topic),
    );
    if (resRemoveDeprecated) {
      console.log(`Removed deprecated tag from topic ${topic.id}`);
    }
  }
  return res;
};

const uploadImg = async (imgPath) => {
  // in rare cases this fails, trying multiple times should reduce error rate close to zero
  const tries = 3;
  for (let i = 0; i < tries; i++) {
    const url = await uploadImgBody(imgPath);
    if (url) return url;
  }
  console.warn("failed to upload", imgPath);
  return undefined;
};

const uploadImgBody = async (imgPath) => {
  if (imgPath.includes("http://")) return imgPath;

  const name = imgPath.slice(Math.max(0, imgPath.lastIndexOf("/") + 1));
  const image = await readFile(imgPath);
  const form = new FormData();
  form.append("synchronous", "true");
  form.append("type", "composer");
  form.append("file", image, { filename: name });

  const res = await http.post("uploads.json", form);
  if (res) console.log("finished uploading " + name);
  else console.log("failed upload " + name);
  const url = res?.data.url; //TODO maybe some default url
  if (!url) {
    console.log(res);
  }
  return url;
};

const createCategory = async (name) => {
  const newCat = { name, parent_category_id: categoryId };
  const res = await http.post("categories.json", JSON.stringify(newCat));
  const createdCat = res?.data.category;
  subCategories[createdCat.name] = createdCat;
  return res;
};

const addDeleteTagToTopic = async (topic) => {
  if (topic.tags.includes("deprecated")) return undefined;
  topic.tags = topic.tags.concat(["deprecated"]);
  const res = await http.put(`t/-/${topic.id}.json`, JSON.stringify(topic));
  console.log(`tagged topic ${topic.id} as deprecated`);
  return res;
};

/**
 * Execution
 */

const uploadAllDocs = async () => {
  await setup();

  // Create sub categories that doesn't exist
  const catagoriesToCreate = uploads.filter((e) => !subCategories[e.name]);
  await Promise.all(catagoriesToCreate.map((e) => createCategory(e.name)));

  // upload documentation for each sub category
  const dirsWithId = addSlugAndIdToDir(uploads);
  await Promise.all(dirsWithId.map((e) => syncToAsk(e)));
};

const syncToAsk = async (dir) => {
  const documentation = await recReader(dir);

  /* 1. get all topics */
  const topics = await getTopics(dir);
  if (!topics) throw "Didn't find any topics";
  const existingTopics = keyBy(topics, "title");

  const toUpload = documentation.filter((e) => !existingTopics[e.title]);
  const toEdit = documentation.filter((e) => existingTopics[e.title])
    .map((e) => {
      return { ...e, id: existingTopics[e.title].id };
    });

  /* 2. create all that doesn't exist */
  await Promise.all(
    toUpload.map((t) => createTopic(dir.id, t.title, t.text)),
  );

  /* 3. edit all that does */
  await Promise.all(toEdit.map((t) => editFirstPostOnTopic(t.id, t.text)));

  /* 4. Add delete tag to things not updated */
  if (tagDeprecatedTopics) {
    const justUploaded = keyBy(documentation, "title");
    const toDelete = topics.filter((e) => !justUploaded[e.title])
      .filter((e) => !/About the .* category/.test(e.title))
      .filter((e) => !(ignore.includes(e.title)));
    await Promise.all(toDelete.map((t) => addDeleteTagToTopic(t)));
  }
};

uploadAllDocs();
